/*
 * files.h
 *
 *  Created on: 18 de jan de 2019
 *      Author: trinovati
 */

#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#ifndef MAIN_FILES_FILES_H_
#define MAIN_FILES_FILES_H_


bool fileExists(char *cpfileName)
{
    int errcode = access(cpfileName, F_OK);
    if (errcode == ENOENT)
        return false; /* nao existe */
    return true; /* existe */
}
bool emptyFile(FILE* file){
	fseek (file, 0, SEEK_END);
	int size = ftell(file);
	if (size == 0) {
		return true;
	} else {
		return false;
	}
}
int fileSize(FILE *file){
	fseek(file, 0L,SEEK_END);
	return ftell(file);
}
void printFile(FILE *file){
	int c;
	do{
		c = fgetc(file);
	    if(c != EOF)
	    	printf("%c", c);
	}while(c != EOF);
}





#endif /* MAIN_FILES_FILES_H_ */
