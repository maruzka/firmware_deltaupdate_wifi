#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include <string.h>
#include "esp_partition.h"
#include "esp_log.h"
#include "esp_http_client.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_spiffs.h"

#include "esp_ota_ops.h"
#include "esp_flash_partitions.h"

// Bibliotecas necessárias para a atualização do firmware
#include "esp_https_ota.h"
#include <unistd.h>

#include "janpatch/include/janpatch.h"
#include "files/files.h"

#define SSID "Trinovati"
#define PASSWORD "Trin0vat1"

#define BUFFSIZE 1024

static const char *TAG = "testewifi";
int cont = 0;

static EventGroupHandle_t wifi_event_group;


static EventGroupHandle_t downloadedFiles;
const int PATCH_FLAG = BIT1;
const int FIRMWARE_FLAG = BIT2;
const int FLAG_JANPATCH = BIT3;

const int CONNECTED_BIT = BIT0;

#define FIRMWARE_DIR "/spiffs/firmware.bin"
#define PATCH_DIR "/spiffs/patch.patch"
#define NEWFIRMWARE_DIR "/spiffs/newfirmware.bin"


esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}


static void __attribute__((noreturn)) task_fatal_error()
{
    ESP_LOGE(TAG, "Exiting task due to fatal error...");
    (void)vTaskDelete(NULL);

    while (1) {
//;
    }
}

static esp_err_t event_handler_wifi(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

int config_wifi(){

	   tcpip_adapter_init();
	   wifi_event_group = xEventGroupCreate();
	   ESP_ERROR_CHECK( esp_event_loop_init(event_handler_wifi, NULL) )
	   wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	   ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
	   ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
	   ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );

	   wifi_config_t sta_config = {
	       .sta = {
	           .ssid =SSID,
	           .password = PASSWORD,
	           .bssid_set = 0
	       }
	   };

	   ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &sta_config) );
	   ESP_ERROR_CHECK( esp_wifi_start() );
	   ESP_ERROR_CHECK( esp_wifi_connect() );
	   ESP_LOGI(TAG, "DEU CERTO");


	   return 1;
}

void spiffs_init(){

   ESP_LOGI(TAG, "Initializing SPIFFS");
   esp_vfs_spiffs_conf_t conf = {
     .base_path = "/spiffs",
     .partition_label = "storage",
     .max_files = 5,
     .format_if_mount_failed = true
   };
   // Use settings defined above to initialize and mount SPIFFS filesystem.
   // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
   esp_err_t ret = esp_vfs_spiffs_register(&conf);
   if(ret == ESP_OK){
	   ESP_LOGI(TAG, "Spiffs montada");
   }else if (ret == ESP_FAIL) {
           ESP_LOGE(TAG, "Failed to mount or format filesystem (%s)", esp_err_to_name(ret));
       } else if (ret == ESP_ERR_NOT_FOUND) {
           ESP_LOGE(TAG, "Failed to find SPIFFS partition");
       } else {
           ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
       }
       return;

}
void spiffs_info(){
	size_t total = 0, used = 0, ret;
	ret = esp_spiffs_info("storage", &total, &used);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
	} else {
		ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
	}
}

static void trv_OTAA_update(void *pvParameters)
{

	xEventGroupWaitBits(downloadedFiles, FLAG_JANPATCH, false, true, portMAX_DELAY);

	FILE *filePtr = fopen(NEWFIRMWARE_DIR, "rb");
	if(filePtr == NULL){
		ESP_LOGE(TAG,"ERRO");
	}
	esp_err_t err;

	esp_ota_handle_t update_handle = 0 ;
	const esp_partition_t *update_partition = NULL;
	const esp_partition_t *configured = esp_ota_get_boot_partition();
	const esp_partition_t *running = esp_ota_get_running_partition();
	unsigned char fileBuffer[1024] = {0};
	printf("Starting OTAA\n"); //DEBUG

	const char TAG[] = "trv_ota";

	//Check if current set boot partition is the same as running partition (should be the same on fresh restart)
	if(configured != running)
	{
		ESP_LOGW("trv_ota", "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x \n",
				configured->address, running->address); //DEBUG
	}
	else
		printf("OTAA_UPDATE is OK to start"); //DEBUG

	update_partition = esp_ota_get_next_update_partition(NULL);
	printf("New partition will be subtype %d at offset 0x%x \n ", update_partition->subtype, update_partition->address); //DEBUG

	 err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
	 if (err != ESP_OK) {
		 ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err)); //DEBUG
		 task_fatal_error();
	     //return ESP_FAIL;
	}
	ESP_LOGI(TAG, "esp_ota_begin succeeded");
	fseek(filePtr, 0, SEEK_SET);


	size_t bytesEscritos = -1;
	while(bytesEscritos != 0){
		bytesEscritos = fread(fileBuffer, sizeof(char), 1024, filePtr);
		ESP_LOGE("bytesEscritos: ", "%d ", bytesEscritos);
		err = esp_ota_write( update_handle, (void *)fileBuffer, bytesEscritos);
	}

	err = esp_ota_end(update_handle);
	if (err == ESP_OK)
	{
		err = esp_ota_set_boot_partition(update_partition);
		    if (err != ESP_OK) {
		        ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
		        task_fatal_error();
		        //return ESP_FAIL;
		    }
		    ESP_LOGI(TAG, "Prepare to restart system!");
		    esp_restart();
		return;
	}
	else
	{
		ESP_LOGE(TAG, "esp_ota_end failed (%s)", esp_err_to_name(err)); //DEBUG
		task_fatal_error();
		//return ESP_FAIL;
	}
	vTaskDelete(NULL);
}

void arquivo_update(char *dados, int len, char *caminho){

	FILE *buffer;
	char buff[len];
	memcpy(buff, dados, len);
	if(esp_spiffs_mounted("storage")){ // talvez seja redundante
		buffer = fopen(caminho, "ab");
		if(buffer == NULL){
			ESP_LOGE(TAG, "Erro ao abrir arquivo %s" , caminho);
		}
	 	fwrite(buff, sizeof(char), len, buffer);
		fclose(buffer);
	}else{
		ESP_LOGI(TAG,"iPartição SPIFFS NAO esta montada. Retornando");
	}
}


void progress(uint8_t percentage) {
    printf("Patch progress: %d%%\n", percentage);
}
void aplicandoPatch(void *pvParameters){


	xEventGroupWaitBits(downloadedFiles, (PATCH_FLAG | FIRMWARE_FLAG), false, true, portMAX_DELAY);


    FILE *source = fopen(FIRMWARE_DIR, "rb");
    FILE *patch  = fopen(PATCH_DIR, "rb");
    FILE *target = fopen(NEWFIRMWARE_DIR, "wb");
    if(source == NULL ){
    	ESP_LOGI(TAG, "Erro source");
    }
    if(patch == NULL ){
    	ESP_LOGI(TAG, "Erro patch");
    }
    if(target == NULL ){
      	ESP_LOGI(TAG, "Erro target");
    }

      printf("Patching...\n");
     // janpatch_ctx contains buffers, and references to the file system functions
     janpatch_ctx ctx = {
    { (unsigned char*)malloc(1024), 1024},
    { (unsigned char*)malloc(1024), 1024},
	{ (unsigned char*)malloc(1024), 1024},
        &fread,
        &fwrite,
        &fseek,
		&ftell,
        &progress
     };

     int jpr = janpatch(ctx, source, patch, target);
     if (jpr != 0) {
         printf("Patching failed... (%d)\n", jpr);
         //xEventGroupSetBits()
     }



     ESP_LOGE(TAG, "%d", fileSize(target));

     // Close files
     fclose(source);
     fclose(patch);
     fclose(target);

	 xEventGroupSetBits(downloadedFiles, FLAG_JANPATCH);


	vTaskDelete(NULL);


}


static void http_cleanup(esp_http_client_handle_t client){
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
}


static void downloadFirmwareTask(void *pvParameter){

    	esp_err_t err;
    	char *TAG = "DFT";
    	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
    	static char ota_write_data[BUFFSIZE + 1] = { 0 };
    	ESP_LOGI(TAG, "Conectado a rede WIFI. Conectando ao servidor");

    	esp_http_client_config_t config = {
			.url = "http://192.168.2.2:8070/hello-world-antigo.bin",
		};
		esp_http_client_handle_t client = esp_http_client_init(&config);
		if (client == NULL) {
			ESP_LOGE(TAG, "Failed to initialise HTTP connection");
			task_fatal_error();
		}
		err = esp_http_client_open(client, 0);
		if (err != ESP_OK) {
			ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
			esp_http_client_cleanup(client);
			task_fatal_error();
		}
		int tam_arq = esp_http_client_fetch_headers(client);

		ESP_LOGI(TAG, "%d", tam_arq);
		int binary_file_length = 0;
		while (1) {

			int data_read = esp_http_client_read(client, ota_write_data, BUFFSIZE);
			if (data_read < 0) {
				ESP_LOGE(TAG, "Error: SSL data read error");
				http_cleanup(client);
				task_fatal_error();
			} else if (data_read > 0) {
				/*if (image_header_was_checked == false) {*/
				//printf("%s", ota_write_data);
					arquivo_update(ota_write_data, data_read, FIRMWARE_DIR);
					binary_file_length += data_read;
					ESP_LOGI(TAG, "Written image length %d, data_read %d", binary_file_length, data_read);
				//} else
				if(binary_file_length == tam_arq) {
					ESP_LOGI(TAG, "Connection closed,all data received");
					xEventGroupSetBits(downloadedFiles, FIRMWARE_FLAG);
				      spiffs_info();

					http_cleanup(client);
					break;
				}
			}
			//ESP_LOGI("ota_task", "%d", binary_file_length);
			vTaskDelay(4);
		}
		ESP_LOGI(TAG, "Total Write binary data length : %d", binary_file_length);

		vTaskDelete(NULL);
}


static void downloadPatchTask(void *pvParameter){

    	esp_err_t err;
    	char *TAG = "DPT";

    	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
    	static char ota_write_data[BUFFSIZE + 1] = { 0 };

    	ESP_LOGI(TAG, "Conectado a rede WIFI. Conectando ao servidor");

    	esp_http_client_config_t config = {
			.url = "http://192.168.2.2:8070/firmware_patch.patch",
		};
		esp_http_client_handle_t client = esp_http_client_init(&config);
		if (client == NULL) {
			ESP_LOGE(TAG, "Failed to initialise HTTP connection");
			task_fatal_error();
		}
		err = esp_http_client_open(client, 0);
		if (err != ESP_OK) {
			ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
			esp_http_client_cleanup(client);
			task_fatal_error();
		}
		int tam_arq = esp_http_client_fetch_headers(client);

		ESP_LOGI(TAG, "%d", tam_arq);
		int binary_file_length = 0;
		while (1) {

			int data_read = esp_http_client_read(client, ota_write_data, BUFFSIZE);
			if (data_read < 0) {
				ESP_LOGE(TAG, "Error: SSL data read error");
				http_cleanup(client);
				task_fatal_error();
			} else if (data_read > 0) {
				/*if (image_header_was_checked == false) {*/
				//printf("%s", ota_write_data);
					arquivo_update(ota_write_data, data_read, PATCH_DIR);
					binary_file_length += data_read;
					ESP_LOGI(TAG, "Written image length %d, data_read %d", binary_file_length, data_read);
				//} else
				if(binary_file_length == tam_arq) {
					ESP_LOGI(TAG, "Connection closed,all data received");
					xEventGroupSetBits(downloadedFiles, PATCH_FLAG);

				      spiffs_info();

					http_cleanup(client);
					break;
				}
			}
			vTaskDelay(4);
		}
		ESP_LOGI(TAG, "Total Write binary data length : %d", binary_file_length);

		vTaskDelete(NULL);
}



void app_main(void){

   // Initialize NVS.
   esp_err_t err = nvs_flash_init();
   if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
	   // OTA app partition table has a smaller NVS partition size than the non-OTA
	   // partition table. This size mismatch may cause NVS initialization to fail.
       // If this happens, we erase NVS partition and initialize NVS again.
       ESP_ERROR_CHECK(nvs_flash_erase());
       err = nvs_flash_init();
    }
      ESP_ERROR_CHECK( err );
      spiffs_init();

      fclose(fopen(FIRMWARE_DIR, "wb"));
      fclose(fopen(PATCH_DIR, "wb"));
      fclose(fopen(NEWFIRMWARE_DIR,"wb"));

      config_wifi();

      downloadedFiles = xEventGroupCreate();
      xEventGroupClearBits(downloadedFiles, FIRMWARE_FLAG | PATCH_FLAG | FLAG_JANPATCH);

      xTaskCreate(&downloadFirmwareTask, "DownloadFirmware", 8192, NULL, 5, NULL);
      xTaskCreate(&downloadPatchTask, "DownloadPatch", 8192, NULL, 5, NULL);
      xTaskCreate(&aplicandoPatch, "aplicandodPatch", 8192, NULL, 5, NULL);
      xTaskCreate(&trv_OTAA_update, "OTAupdate", 8192, NULL,5,NULL);

      //spiffs_info();

}

