deps_config := \
	/home/trinovati/esp/esp-idf/components/app_trace/Kconfig \
	/home/trinovati/esp/esp-idf/components/aws_iot/Kconfig \
	/home/trinovati/esp/esp-idf/components/bt/Kconfig \
	/home/trinovati/esp/esp-idf/components/driver/Kconfig \
	/home/trinovati/esp/esp-idf/components/esp32/Kconfig \
	/home/trinovati/esp/esp-idf/components/esp_adc_cal/Kconfig \
	/home/trinovati/esp/esp-idf/components/esp_http_client/Kconfig \
	/home/trinovati/esp/esp-idf/components/ethernet/Kconfig \
	/home/trinovati/esp/esp-idf/components/fatfs/Kconfig \
	/home/trinovati/esp/esp-idf/components/freertos/Kconfig \
	/home/trinovati/esp/esp-idf/components/heap/Kconfig \
	/home/trinovati/esp/esp-idf/components/libsodium/Kconfig \
	/home/trinovati/esp/esp-idf/components/log/Kconfig \
	/home/trinovati/esp/esp-idf/components/lwip/Kconfig \
	/home/trinovati/esp/esp-idf/components/mbedtls/Kconfig \
	/home/trinovati/esp/esp-idf/components/nvs_flash/Kconfig \
	/home/trinovati/esp/esp-idf/components/openssl/Kconfig \
	/home/trinovati/esp/esp-idf/components/pthread/Kconfig \
	/home/trinovati/esp/esp-idf/components/spi_flash/Kconfig \
	/home/trinovati/esp/esp-idf/components/spiffs/Kconfig \
	/home/trinovati/esp/esp-idf/components/tcpip_adapter/Kconfig \
	/home/trinovati/esp/esp-idf/components/vfs/Kconfig \
	/home/trinovati/esp/esp-idf/components/wear_levelling/Kconfig \
	/home/trinovati/esp/esp-idf/components/bootloader/Kconfig.projbuild \
	/home/trinovati/esp/esp-idf/components/esptool_py/Kconfig.projbuild \
	/home/trinovati/Projetos/firmware_deltaupdate_wifi/main/Kconfig.projbuild \
	/home/trinovati/esp/esp-idf/components/partition_table/Kconfig.projbuild \
	/home/trinovati/esp/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(IDF_CMAKE)" "n"
include/config/auto.conf: FORCE
endif

$(deps_config): ;
